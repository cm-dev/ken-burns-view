# KenBurnsView

_Copied from: <https://github.com/flavioarfaria/KenBurnsView>_

---

- Version: [`1.0.7-beta.7`](version-info.md) _(January 12th, 2016)_
- License: [Apache License v2.0](LICENSE)


## Usage

1. Main `build.gradle`

        :::gradle
        allprojects {
            repositories {
                ...
                maven { url 'https://jitpack.io' }
            }
        }

2. `app/build.gradle` (or similar)

        :::gradle
        dependencies {
            ...
            compile 'org.bitbucket.cm-dev:ken-burns-view:x.x.x'
        }

    _Where `x.x.x` is valid tag name._
