# `1.0.7-beta.7` _(January 12th, 2016)_


## Dependencies

- [`com.android.support:support-annotations:+`][#com.android.support:*]


## Changes

- Fixed method `setEffectEnabled()`.


[#com.android.support:*]: https://developer.android.com/tools/support-library/index.html
